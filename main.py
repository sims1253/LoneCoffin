#!/usr/bin/env python3

from argparse import ArgumentParser

from lone_coffin_questions import questions, game_intro, game_finish, __doc__
from utils import console_print


def parse_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('-w', '--wizard', action='store_true',
                        help="runs the game in wizard mode")

    return parser.parse_args()


def print_question(question: dict):
    """
    Prints the question object and possible responses.
    """
    console_print(question['text'])
    for number, response in enumerate(question['responses'], start=1):
        console_print("{:>3}: {}".format(number, response['text']))


def get_response(max_number: int) -> int:
    """
    Retrieves a response number from the user. The value will be between
    (including) 1 and the given max_number.

    :return: The response from the user converted to int.
    """
    while True:
        try:
            number = int(input("Choose an answer: "))
        except ValueError:
            console_print("Naaah, a number!")
            continue

        if not (0 < number <= max_number):
            console_print("Choose something reasonable!")
            continue

        return number


def main():
    args = parse_args()
    state = game_intro()

    while state.running:

        question = state.next_question(questions)
        print_question(question)
        number = get_response(len(question['responses']))

        print("")
        question['responses'][number - 1]['reaction'](state)

        if args.wizard:
            print(str(state))

    print("")
    game_finish(state)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("Program terminated by user.")
    except EOFError:
        print("EOF detected, terminating gracefully. Goodbye!")
