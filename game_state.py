class GameState:

    def __init__(self):
        self.next = 0

    def next_question(self, questions: dict):
        return questions[self.next]

    @property
    def running(self):
        return self.next is not None
