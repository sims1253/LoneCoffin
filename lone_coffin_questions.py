"""
A textbased adventure in which you need to get rid of a friend so you can watch
you favourite tv show in peace. But be aware! Don't destroy your friendship!
"""

from game_state import GameState
from utils import console_print


class LoneCoffinState(GameState):

    def __init__(self):
        GameState.__init__(self)
        self.friendship = 100
        self.pullout = 0

    def __str__(self):
        return ("Friendship: {self.friendship}\n"
                "Pullout:    {self.pullout}").format(self=self)


def friend_pullout_next(friend_diff: int, pullout_diff: int, next: (int, None), out: str=None):
    """
    Generates a function that affects the game state with the given value.

    :param friend_diff: Difference in friendship.
    :param pullout_diff: Difference in pullout.
    :param next: The next question index.
    :return: A function that does the named stuff.
    """
    def changer(gamestate: LoneCoffinState):
        gamestate.friendship += friend_diff
        gamestate.pullout += pullout_diff
        gamestate.next = next
        if out is not None:
            console_print(out)

    return changer


questions = {
    0: {'text': 'Allan: "Could you pass me a beer?"',
        'responses': [
            {'text': "You've been drinking a lot lately...",
             'reaction': friend_pullout_next(
                 -8, 3, None,
                 '"Are you insane? I had to drag you home the last time we '
                 'were out because you couldn\'t walk anymore.')},
            {'text': 'Sure, catch!',
             'reaction': friend_pullout_next(
                 +15, -10, None,
                 'Allan catches the bottle out of the air, opens it with a '
                 'lighter and chugs.\n"Thanks. I knew you were my man!"')},
            {'text': 'We are out of beer.',
             'reaction': friend_pullout_next(-13, 0, 1)}]},

    1: {'text': 'What are you talking about? I can see them in the fridge!',
        'responses': [
            {'text': "Whoops, you're right.",
             'reaction': friend_pullout_next(0, 0, None)},
            {'text': "True, but none for you!",
             'reaction': friend_pullout_next(0, 0, None)},
            {'text': "You've been drinking a lot lately...",
             'reaction': friend_pullout_next(
                 0, 0, None,
                 '"Are you insane? I had to drag you home the last time we '
                 'were out because you couldn\'t walk anymore.')},
        ]}
}


def game_intro() -> GameState:
    console_print("Finally home! And even better: It's friday. Good things "
                  "happen on fridays! Like every friday you kept your evening "
                  "free of dates and parties and all that other stuff so you "
                  "can watch your favorite TV show. Like you always do. And "
                  "you prefer to do that alone - It's more relaxing this way."
                  "\n\n"
                  "Since it's 5.00 pm you decide to take a nap on your couch "
                  "since the show starts at 18.30. You turn the TV on so you "
                  "can wake up to the opening theme of the show you love so "
                  "much. You fall asleep instantly.\n"
                  "\n"
                  "DING DONG you here the doorbell. Oh no! It's your best "
                  "friend Allan. You look at you watch: It's 18:28 and you "
                  "really want watch this show alone. You have 2 minutes to "
                  "get rid of Allan. But do it nicely: You really like him - "
                  "He is your best friend after all.\n"
                  "\n"
                  "Get rid of Allan politely.")
    return LoneCoffinState()


def game_finish(state: LoneCoffinState):
    """
    Prints the end of the game.

    :param state: The GameState object.
    """
    if state.friendship > 0 and state.pullout >= 100:
        console_print(
            "Allan looks at you with disappointment in his eyes.\n\n"
            '"I think I will leave now. Go, watch you House of breaking '
            'thrones, I don\'t feel welcome here."\n\n'
            'Allan walks out your room and leaves you wondering if your '
            'friendship will recover. As you gaze out the window the alarm '
            'in your cellphone goes off and without even thinking about it '
            'you turn on your TV. The last thing you notice are the opening '
            'tunes of the most epic intro of all time before you are sucked '
            'into the void of enjoyment that is House of breaking thrones.')
    elif state.friendship <= 0:
        console_print(
            "Unfortunately, your friend is not your friend anymore. As he "
            "slams the door he shouts: \"You won't see me again!\" You "
            "really should have behaved better.")
    else:
        console_print(
            "Damn!\n"
            "The show starts but your friend is still here. Something is "
            "not right today. You won't watch your favorite TV show like "
            "you always do!")
