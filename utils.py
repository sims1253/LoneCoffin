def console_print(*args, delimiter=' ', end='\n', limit=80):
    """

    Prints the given stuff - and breaks it word wise when going further than 80
    chars.

    :param args: The stuff you want to have printed.
    :param delimiter: Will be placed between all the args.
    :param limit: Char limit.
    :param end: An ending, will be put as last character and doesn't count into
                the char limit.
    """
    output = delimiter.join(args)
    lines = output.splitlines(keepends=True)
    for line in lines:
        curr_print = line
        while len(curr_print.rstrip('\n')) > limit:
            splitpos = curr_print[:limit].rfind(' ')
            if splitpos < 0:
                # Word too long, search for a space left from limit at least
                splitpos = curr_print.find(' ')
                if splitpos < 0:
                    break  # Break out and print the long thing in 28

            print(curr_print[:splitpos], end='\n')
            curr_print = curr_print[splitpos+1:]

        print(curr_print, end='')

    print(end=end)
